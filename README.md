# nvim

```
sudo apt install git curl wget unzip tar gzip cargo npm cmake pkg-config libfreetype6-dev libfontconfig1-dev libxcb-xfixes0-dev libxkbcommon-dev python3
```

How to install:
```
git clone --recurse-submodules "https://github.com/PERTZ42/nvim"
```
```
git submodule update --recursive --remote
```

### plugins:

* undotree
* treesitter
* telescope
* lsp-config
* mason
* mason-lspconfig
* plenary
* lualine
* gitsigns
* cmp (buffer, calc, luasnip, nerdfont, nvim-lsp, path, tmux)
* dap
* fiendly-snippets
* vim-tmux-navigator


all plugins inside pack/plugins/start
settings in lua/config

theme: Catppuccin/Onedark/Rosé Pine

when you want to add a lsp add it to lua/config/lsp.lua something like this ``lspconfig.lua_ls.setup {}``
