require("config.vimsets")
require("config.vimkeys")

-- plugins configs --
require("config.telescope")
require("config.treesitter")
require("config.undotree")
require("config.lsp")
require("config.colors")
require("config.lspkeys")
require("config.gitsigns")
require("config.lualine")
require("config.cmp")
require("config.dap")
