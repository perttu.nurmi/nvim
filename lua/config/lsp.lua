local lspconfig = require('lspconfig')
require("mason").setup()
require("mason-lspconfig").setup()
local capabilities = require('cmp_nvim_lsp').default_capabilities()

-- After setting up mason-lspconfig you may set up servers via lspconfig
lspconfig.lua_ls.setup {
  on_attach = on_attach,
  capabilities = capabilities,
  settings = {
    Lua = {
      runtime = {
        -- Tell the language server which version of Lua you're using (most likely LuaJIT in the case of Neovim)
        version = "LuaJIT",
        path = vim.split(package.path, ";"),
      },
      diagnostics = {
        -- Get the language server to recognize the `vim` global
        globals = { "vim" },
      },
      workspace = {
        -- Make the server aware of Neovim runtime files and plugins
        library = { vim.env.VIMRUNTIME },
        checkThirdParty = false,
      },
      telemetry = {
        enable = false,
      },
      library = {
        '${3rd}/luv/library',
        unpack(vim.api.nvim_get_runtime_file('', true)),
      },
      completion = {
        callSnippet = 'Replace',
      },
      capabilities = capabilities
    },
  },
}
lspconfig.pylsp.setup {
  on_attach = custom_attach,
  settings = {
    pylsp = {
      plugins = {
        -- formatter options
        black = { enabled = true },
        autopep8 = { enabled = true },
        yapf = { enabled = true },
        -- linter options
        pylint = { enabled = false, executable = "pylint" },
        pyflakes = { enabled = true },
        pycodestyle = { enabled = true },
        -- type checker
        pylsp_mypy = { enabled = true },
        -- auto-completion options
        jedi_completion = { fuzzy = true },
        -- import sorting
        pyls_isort = { enabled = true },
        capabilities = capabilities,
      },
    },
  },
  flags = {
    debounce_text_changes = 200,
  },
  capabilities = capabilities,
}
lspconfig.bashls.setup {
  on_attach = custom_attach,
  capabilities = capabilities,
}
lspconfig.gopls.setup {
  on_attach = custom_attach,
  capabilities = capabilities,
}
lspconfig.clangd.setup {
  on_attach = custom_attach,
  capabilities = capabilities,
}
lspconfig.vimls.setup {
  on_attach = custom_attach,
  capabilities = capabilities,
}
lspconfig.html.setup {
  on_attach = custom_attach,
  capabilities = capabilities,
}
--lspconfig.grammarly.setup {
--        capabilities = capabilities,
--}
lspconfig.cssls.setup {
  on_attach = custom_attach,
  capabilities = capabilities,
}
lspconfig.awk_ls.setup {
  on_attach = custom_attach,
  capabilities = capabilities,
}
lspconfig.cmake.setup {
  on_attach = custom_attach,
  capabilities = capabilities,
}
lspconfig.rust_analyzer.setup {
  on_attach = custom_attach,
  capabilities = capabilities,
  cmd = {
    "rustup", "run", "stable", "rust-analyzer",
    capabilities = capabilities,
    vim.keymap.set("n", "<leader>cr", ":Cargo run\n")
  }
}
--lspconfig.texlab.setup {
--        capabilities = capabilities,
--}
--lspconfig.marksman.setup {
--  on_attach = custom_attach,
--  capabilities = capabilities,
--}

lspconfig.tsserver.setup {
  on_attach = custom_attach,
  capabilities = capabilities,
}

lspconfig.csharp_ls.setup {
  on_attach = custom_attach,
  capabilities = capabilities,
}
