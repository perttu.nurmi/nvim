function OneDark()
  require('onedark').setup {
    style = 'light',
    transparent = true,           -- Show/hide background
    term_colors = false,          -- Change terminal color as per the selected theme style
    ending_tildes = false,        -- Show the end-of-buffer tildes. By default they are hidden
    cmp_itemkind_reverse = false, -- reverse item kind highlights in cmp menu

    -- toggle theme style ---
    toggle_style_key = nil,                                                              -- keybind to toggle theme style. Leave it nil to disable it, or set it to a string, for example "<leader>ts"
    toggle_style_list = { 'dark', 'darker', 'cool', 'deep', 'warm', 'warmer', 'light' }, -- List of styles to toggle between

    -- Change code style ---
    -- Options are italic, bold, underline, none
    -- You can configure multiple style with comma separated, For e.g., keywords = 'italic,bold'
    code_style = {
      comments = 'italic',
      keywords = 'bold',
      functions = 'none',
      strings = 'italic',
      variables = 'none'
    },

    -- Lualine options --
    lualine = {
      transparent = true, -- lualine center bar transparency
    },

    -- Custom Highlights --
    colors = {
      bright_orange = "ff8800", -- define a new color
      --green = '#00ffaa',            -- redefine an existing color
    },                          -- Override default colors
    highlights = {
      --["@keyword"] = {fg = '$green'},
      --["@string"] = {fg = '$bright_orange', bg = '#00ff00', fmt = 'bold'},
      --["@function"] = {fg = '#0000ff', sp = '$cyan', fmt = 'underline,italic'},
      --["@function.builtin"] = { fg = '$bright_orange' }
    }, -- Override highlight groups

    -- P-lugins Config --
    diagnostics = {
      darker = true,     -- darker colors for diagnostic
      undercurl = true,  -- use undercurl instead of underline for diagnostics
      background = true, -- use background color for virtual text
    },
  }
  require('onedark').load()
end

function Catppuccin()
  require("catppuccin").setup({
    flavour = "auto", -- latte, frappe, macchiato, mocha, auto
    background = {    -- :h background
      light = "latte",
      dark = "mocha",
    },
    transparent_background = true, -- disables setting the background color.
    show_end_of_buffer = true,     -- shows the '~' characters after the end of buffers
    term_colors = false,           -- sets terminal colors (e.g. `g:terminal_color_0`)
    dim_inactive = {
      enabled = false,             -- dims the background color of inactive window
      shade = "dark",
      percentage = 0.15,           -- percentage of the shade to apply to the inactive window
    },
    no_italic = false,             -- Force no italic
    no_bold = false,               -- Force no bold
    no_underline = false,          -- Force no underline
    styles = {                     -- Handles the styles of general hi groups (see `:h highlight-args`):
      comments = { "italic" },     -- Change the style of comments
      conditionals = { "italic" },
      loops = {},
      functions = {},
      keywords = {},
      strings = { "italic" },
      variables = {},
      numbers = {},
      booleans = { "bold" },
      properties = {},
      types = {},
      operators = {},
      -- miscs = {}, -- Uncomment to turn off hard-coded styles
    },
    color_overrides = {},
    custom_highlights = {},
    default_integrations = true,
    integrations = {
      cmp = true,
      gitsigns = true,
      nvimtree = true,
      treesitter = true,
      notify = false,
      mini = {
        enabled = true,
        indentscope_color = "",
      },
      -- For more plugins integrations please scroll down (https://github.com/catppuccin/nvim#integrations)
    },
  })

  -- setup must be called before loading
  --  vim.cmd.colorscheme "catppuccin"
end

require("rose-pine").setup({
  variant = "auto",      -- auto, main, moon, or dawn
  dark_variant = "main", -- main, moon, or dawn
  dim_inactive_windows = false,
  extend_background_behind_borders = false,

  enable = {
    terminal = true,
    legacy_highlights = true, -- Improve compatibility for previous versions of Neovim
    migrations = true,        -- Handle deprecated options automatically
  },

  styles = {
    bold = true,
    italic = true,
    transparency = true,
  },

  groups = {
    border = "muted",
    link = "iris",
    panel = "surface",

    error = "love",
    hint = "iris",
    info = "foam",
    note = "pine",
    todo = "rose",
    warn = "gold",

    git_add = "foam",
    git_change = "rose",
    git_delete = "love",
    git_dirty = "rose",
    git_ignore = "muted",
    git_merge = "iris",
    git_rename = "pine",
    git_stage = "iris",
    git_text = "rose",
    git_untracked = "subtle",

    h1 = "iris",
    h2 = "foam",
    h3 = "rose",
    h4 = "gold",
    h5 = "pine",
    h6 = "foam",
  },

  highlight_groups = {
    -- Comment = { fg = "foam" },
    -- VertSplit = { fg = "muted", bg = "muted" },
  },

  before_highlight = function(group, highlight, palette)
    -- Disable all undercurls
    -- if highlight.undercurl then
    --     highlight.undercurl = false
    -- end
    --
    -- Change palette colour
    -- if highlight.fg == palette.pine then
    --     highlight.fg = palette.foam
    -- end
  end,
})

local monokai = require('monokai')
local palette = monokai.classic
monokai.setup {
  palette = {
    name = 'monokai',
    base1 = '#272a30',
    base2 = '#26292C',
    base3 = '#2E323C',
    base4 = '#333842',
    base5 = '#4d5154',
    base6 = '#9ca0a4',
    base7 = '#b1b1b1',
    border = '#a1b5b1',
    brown = '#504945',
    white = '#f8f8f0',
    grey = '#8F908A',
    black = '#000000',
    pink = '#f92672',
    green = '#a6e22e',
    aqua = '#66d9ef',
    yellow = '#e6db74',
    orange = '#fd971f',
    purple = '#ae81ff',
    red = '#e95678',
    diff_add = '#3d5213',
    diff_remove = '#4a0f23',
    diff_change = '#27406b',
    diff_text = '#23324d',
  },
  custom_hlgroups = {
    TSInclude = {
      fg = palette.aqua,
    },
    GitSignsAdd = {
      fg = palette.green,
      bg = palette.base2
    },
    GitSignsDelete = {
      fg = palette.pink,
      bg = palette.base2
    },
    GitSignsChange = {
      fg = palette.orange,
      bg = palette.base2
    },
  }
}

-- vim.cmd("colorscheme rose-pine-main")
-- vim.cmd("colorscheme rose-pine-moon")
-- vim.cmd("colorscheme rose-pine-dawn")
--
--Catppuccin()
-- vim.cmd("colorscheme rose-pine")

function TransparentBg()
  vim.api.nvim_set_hl(0, "Normal", { bg = "none" })
  vim.api.nvim_set_hl(0, "NormalFloat", { bg = "none" })
  vim.api.nvim_set_hl(0, "TelescopeNormal", { bg = "none" })
  vim.api.nvim_set_hl(0, "SignColumn", { bg = "none" })
  vim.api.nvim_set_hl(0, "LineNr", { bg = "none", fg = '#cc6666' })
  vim.api.nvim_set_hl(0, "FloatBorder", { bg = "none" })
end

function DarkBg()
  vim.api.nvim_set_hl(0, "Normal", { bg = "BLACK" })
  vim.api.nvim_set_hl(0, "NormalFloat", { bg = "BLACK" })
end

vim.cmd.colorscheme("colorbuddy")
-- or
vim.cmd.colorscheme("gruvbuddy")

-- file: colors/my-colorscheme-name.lua

local colorbuddy = require('colorbuddy')

-- Set up your custom colorscheme if you want
--colorbuddy.colorscheme("gruvbuddy")

-- And then modify as you like
local Color = colorbuddy.Color
local colors = colorbuddy.colors
local Group = colorbuddy.Group
local groups = colorbuddy.groups
local styles = colorbuddy.styles

-- Use Color.new(<name>, <#rrggbb>) to create new colors
-- They can be accessed through colors.<name>
Color.new('background', '#282c34')
Color.new('red', '#cc6666')
Color.new('green', '#99cc99')
Color.new('yellow', '#f0c674')

-- Define highlights in terms of `colors` and `groups`
Group.new('Function', colors.yellow, colors.background, styles.bold)
Group.new('luaFunctionCall', groups.Function, groups.Function, groups.Function)

-- Define highlights in relative terms of other colors
Group.new('Error', colors.red:light(), nil, styles.bold)

-- If you want multiple styles, just add them!
Group.new('italicBoldFunction', colors.green, groups.Function, styles.bold + styles.italic)

-- If you want the same style as a different group, but without a style: just subtract it!
-- Group.new('boldFunction', colors.yellow, colors.background, groups.italicBoldFunction - styles.italic)

--vim.cmd("colorscheme monokai_ristretto")
--vim.cmd("colorscheme gruvbox")
TransparentBg()
