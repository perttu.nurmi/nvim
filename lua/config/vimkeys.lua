vim.g.mapleader = " "
vim.keymap.set("n", "<leader>pv", vim.cmd.Ex)

-- Highlight yanked line
local highlight_group = vim.api.nvim_create_augroup('YankHighlight', { clear = true })
vim.api.nvim_create_autocmd('TextYankPost', {
  callback = function()
    vim.highlight.on_yank()
  end,
  group = highlight_group,
  pattern = '*',
})

vim.keymap.set("n", "<leader>ww", vim.cmd.write)
vim.keymap.set("n", "<leader>xx", vim.cmd.source)

vim.keymap.set("n", "<leader>kw", ":q\n")
