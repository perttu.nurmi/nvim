local opt = vim.opt

vim.opt.ttimeoutlen = 100
vim.opt.timeoutlen = 1000

vim.opt.nu = true
vim.opt.relativenumber = true

vim.opt.tabstop = 2
vim.opt.softtabstop = 2
vim.opt.shiftwidth = 2
vim.opt.expandtab = true

vim.opt.smartindent = true

vim.opt.wrap = false

vim.opt.swapfile = false
vim.opt.backup = false
vim.opt.undodir = os.getenv("HOME") .. "/.vim/undodir"
vim.opt.undofile = true

vim.opt.hlsearch = false
vim.opt.incsearch = true

vim.opt.termguicolors = true

vim.opt.scrolloff = 999
vim.opt.signcolumn = "yes"
vim.opt.isfname:append("@-@")

vim.opt.updatetime = 50

vim.opt.colorcolumn = "80"

vim.o.laststatus = 3

-- automatically change directory
vim.o.autochdir = false

vim.opt.showmode = true
vim.opt.ignorecase = true -- Ignore case when searching...
vim.opt.smartcase = true  -- ... unless there is a capital letter in the query
vim.opt.showcmd = true
vim.opt.cmdheight = 1     -- Height of the command bar
vim.opt.showmatch = true  -- show matching brackets when text indicator is over them
opt.hidden = true         -- I like having buffers stay around
opt.equalalways = false   -- I don't like my windows changing all the time
opt.splitright = true     -- Prefer windows splitting to the right
opt.splitbelow = true     -- Prefer windows splitting to the bottom
opt.updatetime = 1000     -- Make updates happen faster
--opt.hlsearch = true       -- I wouldn't use this without my DoNoHL function

-- Cool floating window popup menu for completion on command line
--opt.pumblend = 17
--opt.wildmode = "longest:full"
--opt.wildoptions = "pum"

-- Cursorline highlighting control
--  Only have it on in the active buffer
opt.cursorline = true -- Highlight the current line
local group = vim.api.nvim_create_augroup("CursorLineControl", { clear = true })
local set_cursorline = function(event, value, pattern)
  vim.api.nvim_create_autocmd(event, {
    group = group,
    pattern = pattern,
    callback = function()
      vim.opt_local.cursorline = value
    end,
  })
end
set_cursorline("WinLeave", false)
set_cursorline("WinEnter", true)
set_cursorline("FileType", false, "TelescopePrompt")

opt.belloff = "all" -- Just turn the dang bell off

vim.opt.clipboard = 'unnamedplus'
